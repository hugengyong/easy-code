import Vue from 'vue'
import iView from 'iview'
import i18n from '@/locale'
import config from '@/config'
import importDirective from '@/directive'
import router from './router'
// import consts from '@/utils/consts'
// import { appRouter } from '@/router/iviewAdmin'
// import { adminRouter } from './router/routes'
import store from '@/store'
import App from './app.vue'

import './index.less'
import '@/assets/icons/iconfont.css'

if (config.mock) require('@/mock')

Vue.use(iView, {
  i18n: (key, value) => i18n.t(key, value)
})

Vue.config.productionTip = false
/**
 * @description 全局注册应用配置
 */
Vue.prototype.$config = config
/**
 * 注册指令
 */
importDirective(Vue)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  i18n,
  store,
  render: h => h(App)
  // , TODO 选项栏处理
  // data: {
  //   currentPageName: ''
  // },
  // mounted () {
  //   this.currentPageName = this.$route.name
  //   // 显示打开的页面的列表
  //   this.$store.commit('setOpenedList')
  //   this.$store.commit('initCachepage')
  //   // 权限菜单过滤相关
  //   this.$store.commit('updateMenulist')
  // },
  // created () {
  //   let tagsList = []
  //   if (consts.IVIEW_ADMIN_MENU) {
  //     appRouter.map((item) => {
  //       if (item.children.length <= 1) {
  //         tagsList.push(item.children[0])
  //       } else {
  //         tagsList.push(...item.children)
  //       }
  //     })
  //   } else {
  //     adminRouter.map((item) => {
  //       if (item.children.length <= 1) {
  //         tagsList.push(item.children[0])
  //       } else {
  //         tagsList.push(...item.children)
  //       }
  //     })
  //   }
  //   this.$store.commit('setTagsList', tagsList)
  // }
})
