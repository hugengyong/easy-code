import Mock from 'mockjs'
import { userInfo, login, logout } from './login'
import { getDragList, getTableData } from './data'

// 登录相关和获取用户信息
Mock.mock(/\/login/, login)
Mock.mock(/\/userInfo/, userInfo)
Mock.mock(/\/logout/, logout)
Mock.mock(/\/get_table_data/, getTableData)
Mock.mock(/\/get_drag_list/, getDragList)

export default Mock
