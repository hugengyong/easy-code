export default {
  /**
   * @description token在Cookie中存储的天数，默认1天
   */
  cookieExpires: 1,

  /**
   * @description 是否使用国际化，默认为false
   *              如果不使用，则需要在路由中给需要在菜单中展示的路由设置meta: {title: 'xxx'}
   *              用来在菜单中显示文字
   */
  useI18n: true,

  /**
   * @description 是否访问mock接口
   */
  mock: false,

  /**
   * @description 是否校验用户菜单
   */
  checkUserMenu: true,

  /**
   * @description api请求基础路径
   */
  baseUrl: {
    dev: 'http://localhost:8080/',
    pro: 'http://localhost:8080/'
  }
}
