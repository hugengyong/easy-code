import Vue from 'vue'
import iView from 'iview'
import VueRouter from 'vue-router'
import store from '@/store'
import config from '@/config'
import { iviewAdminRouter } from './iviewAdmin'
import { adminRouter } from './routes'
import { canTurnTo, getToken } from '@/libs/util'

Vue.use(VueRouter)

// 所有上面定义的路由都要写在下面的routers里
export const routers = [
  ...adminRouter,
  ...iviewAdminRouter
]

const router = new VueRouter({
  routes: routers,
  // 当hashbang的值为true时，所有的路径都会被格式化已#!开头
  hashbang: true,
  history: true,
  saveScrollPosition: true,
  suppressTransitionError: true
})

const LOGIN_ROUTER_NAME = 'login'
const HOME_ROUTER_NAME = 'home'

const turnTo = (to, next, user) => {
  // 拉取用户信息，通过用户权限和跳转的页面的name来判断是否有权限访问;access必须是一个数组，如：['super_admin'] ['super_admin', 'admin']
  if (canTurnTo(to.name, [user.access], routers)) {
    if (config.checkUserMenu && to.name !== LOGIN_ROUTER_NAME && to.name !== HOME_ROUTER_NAME && !store.state.loginStore.menuOptionMap[to.path]) {
      // 有权限，继续访问
      iView.LoadingBar.start()
      store.dispatch('listUserMenuOperation', {
        params: {
          menuPath: to.path
        }
      }).then(() => {
        next()
      })
    } else {
      // 有权限，继续访问
      iView.LoadingBar.start()
      next()
    }
  } else {
    // 无权限，重定向到401页面
    iView.LoadingBar.start()
    next({
      replace: true,
      name: 'error_401'
    })
  }
}

router.beforeEach((to, from, next) => {
  const token = getToken()
  if (!token && to.name !== LOGIN_ROUTER_NAME) {
    // 未登录且要跳转的页面不是登录页
    iView.LoadingBar.start()
    next({
      name: LOGIN_ROUTER_NAME // 跳转到登录页
    })
  } else if (!token && to.name === LOGIN_ROUTER_NAME) {
    // 未登陆且要跳转的页面是登录页
    iView.LoadingBar.start()
    next() // 跳转
  } else if (token && to.name === LOGIN_ROUTER_NAME) {
    // 已登录且要跳转的页面是登录页 & 跳转到home页
    if (from.name === HOME_ROUTER_NAME) {
      next(false)
    } else {
      iView.LoadingBar.start()
      next({
        name: HOME_ROUTER_NAME
      })
    }
  } else {
    // 已登录且要跳转的页面不是登录页
    if (!store.state.iviewAdminUserStore.hasGetInfo) {
      // 刷新页面时，重新获取当前登录用户信息
      store.dispatch('userInfo').then(() => {
        let user = store.state.loginStore.user
        store.commit('setToken', user.token)
        store.commit('setAvator', user.avator)
        store.commit('setUserName', user.name)
        store.commit('setAccess', user.access)
        store.commit('setHasGetInfo', true)
        turnTo(to, next, user)
      })
    } else {
      let user = store.state.loginStore.user
      turnTo(to, next, user)
    }
  }
})

router.afterEach((to) => {
  iView.LoadingBar.finish()
  window.scrollTo(0, 0)
})

export default router
