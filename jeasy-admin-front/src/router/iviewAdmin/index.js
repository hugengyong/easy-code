import Main from '@/components/main'
import parentView from '@/components/parent-view'

/**
 * iview-admin中meta除了原生参数外可配置的参数:
 * meta: {
 *  hideInMenu: (false) 设为true后在左侧菜单不会显示该页面选项
 *  notCache: (false) 设为true后页面不会缓存
 *  access: (null) 可访问该页面的权限数组，当前路由设置的权限会影响子路由
 *  icon: (-) 该页面在左侧菜单、面包屑和标签导航处显示的图标，如果是自定义图标，需要在图标名称前加下划线'_'
 *  beforeCloseName: (-) 设置该字段，则在关闭当前tab页时会去'@/router/before-close.js'里寻找该字段名对应的方法，作为关闭前的钩子函数
 * }
 */
export const iviewAdminRouter = [
  {
    path: '/login',
    name: 'login',
    meta: {
      title: 'Login - 登录',
      hideInMenu: true
    },
    component: () => import('@/views/login/login.vue')
  },
  {
    path: '/',
    name: '_home',
    redirect: '/home',
    component: Main,
    meta: {
      icon: 'md-home',
      title: '首页_V2',
      hideInMenu: true,
      notCache: true
    },
    children: [
      {
        path: '/home',
        name: 'home',
        meta: {
          icon: 'md-home',
          title: '首页_V2',
          hideInMenu: true,
          notCache: true
        },
        component: () => import('@/views/single-page/home')
      },
      {
        path: 'ownspace',
        name: 'ownspace_index',
        meta: {
          title: '个人中心',
          hideInMenu: true
        },
        component: () => import('@/views/own-space/own-space.vue')
      },
      {
        path: 'order/:order_id',
        name: 'order-info',
        meta: {
          title: '订单详情',
          hideInMenu: true
        },
        component: () => import('@/views/advanced-router/component/order-info.vue')
      },
      {
        // 用于展示动态路由
        path: 'shopping',
        name: 'shopping',
        meta: {
          title: '购物详情',
          hideInMenu: true
        },
        component: () => import('@/views/advanced-router/component/shopping-info.vue')
      },
      {
        // 用于展示带参路由
        path: 'message',
        name: 'message_index',
        meta: {
          title: '消息中心',
          hideInMenu: true
        },
        component: () => import('@/views/message/message.vue')
      }
    ]
  },
  {
    path: '/argu',
    name: 'argu',
    meta: {
      hideInMenu: true
    },
    component: Main,
    children: [
      {
        path: 'params/:id',
        name: 'params',
        meta: {
          icon: 'md-flower',
          title: route => `动态路由-${route.params.id}`,
          notCache: true
        },
        component: () => import('@/views/argu-page/params.vue')
      },
      {
        path: 'query',
        name: 'query',
        meta: {
          icon: 'md-flower',
          title: route => `带参路由-${route.query.id}`,
          notCache: true
        },
        component: () => import('@/views/argu-page/query.vue')
      }
    ]
  },
  {
    path: '/401',
    name: 'error_401',
    meta: {
      hideInMenu: true
    },
    component: () => import('@/views/error-page/401.vue')
  },
  {
    path: '/500',
    name: 'error_500',
    meta: {
      hideInMenu: true
    },
    component: () => import('@/views/error-page/500.vue')
  },
  {
    path: '*',
    name: 'error_404',
    meta: {
      hideInMenu: true
    },
    component: () => import('@/views/error-page/404.vue')
  },
  {
    path: '/preview',
    name: 'preview',
    meta: {
      hideInMenu: true
    },
    component: () => import('@/views/form/article-publish/preview.vue')
  },
  {
    path: '/locking',
    name: 'locking',
    meta: {
      hideInMenu: true
    },
    component: () => import('@/views/main-components/lockscreen/components/locking-page.vue')
  },
  {
    path: '/',
    name: 'doc',
    meta: {
      title: '文档',
      href: 'https://lison16.github.io/iview-admin-doc/#/',
      icon: 'ios-book',
      access: ['super_admin']
    }
  },
  {
    path: '/join',
    name: 'join',
    meta: {
      icon: '_qq',
      title: 'QQ群',
      access: ['super_admin']
    },
    component: Main,
    children: [
      {
        path: 'join_page',
        name: 'join_page',
        meta: {
          icon: '_qq',
          title: 'QQ群',
          access: ['super_admin']
        },
        component: () => import('@/views/join-page.vue')
      }
    ]
  },
  {
    path: '/',
    name: 'home_v1',
    meta: {
      icon: 'md-home',
      title: '首页_v1',
      access: ['super_admin']
    },
    component: Main,
    children: [
      {
        path: 'home_v1',
        name: 'home_v1_page',
        meta: {
          icon: 'md-home',
          title: '首页_v1',
          access: ['super_admin']
        },
        component: () => import('@/views/home/home.vue')
      }
    ]
  },
  {
    path: '/component_v1',
    name: 'component_v1',
    meta: {
      icon: 'logo-buffer',
      title: '组件_V1',
      access: ['super_admin']
    },
    component: Main,
    children: [
      {
        path: 'draggable-list',
        name: 'draggable-list',
        meta: {
          icon: 'ios-infinite',
          title: '可拖拽列表',
          access: ['super_admin']
        },
        component: () => import('@/views/my-components/draggable-list/draggable-list.vue')
      },
      {
        path: 'area-linkage',
        name: 'area-linkage',
        meta: {
          icon: 'ios-more',
          title: '城市级联',
          access: ['super_admin']
        },
        component: () => import('@/views/my-components/area-linkage/area-linkage.vue')
      },
      {
        path: 'file-upload',
        name: 'file-upload',
        meta: {
          icon: 'ios-cloud-upload',
          title: '文件上传',
          access: ['super_admin']
        },
        component: () => import('@/views/my-components/file-upload/file-upload.vue')
      },
      {
        path: 'count-to',
        name: 'count-to',
        meta: {
          icon: 'ios-trending-up',
          title: '数字渐变',
          access: ['super_admin']
        },
        component: () => import('@/views/my-components/count-to/count-to.vue')
      },
      {
        path: 'split-pane-page',
        name: 'split-pane-page',
        meta: {
          icon: 'ios-pause',
          title: '分隔窗口',
          access: ['super_admin']
        },
        component: () => import('@/views/my-components/split-pane/split-pane-page.vue')
      }
    ]
  },
  {
    path: '/component_v2',
    name: 'component_v2',
    meta: {
      icon: 'logo-buffer',
      title: '组件_V2',
      access: ['super_admin']
    },
    component: Main,
    children: [
      {
        path: 'count_to_page',
        name: 'count_to_page',
        meta: {
          icon: 'ios-trending-up',
          title: '数字渐变',
          access: ['super_admin']
        },
        component: () => import('@/views/components/count-to/count-to.vue')
      },
      {
        path: 'drag_list_page',
        name: 'drag_list_page',
        meta: {
          icon: 'ios-infinite',
          title: '拖拽列表',
          access: ['super_admin']
        },
        component: () => import('@/views/components/drag-list/drag-list.vue')
      },
      {
        path: 'tables_page',
        name: 'tables_page',
        meta: {
          icon: 'ios-grid',
          title: '多功能表格',
          access: ['super_admin']
        },
        component: () => import('@/views/components/tables/tables.vue')
      },
      {
        path: 'split_pane_page',
        name: 'split_pane_page',
        meta: {
          icon: 'ios-pause',
          title: '分割窗口',
          access: ['super_admin']
        },
        component: () => import('@/views/components/split-pane/split-pane.vue')
      },
      {
        path: 'markdown_page',
        name: 'markdown_page',
        meta: {
          icon: 'logo-markdown',
          title: 'Markdown编辑器',
          access: ['super_admin']
        },
        component: () => import('@/views/components/markdown/markdown.vue')
      },
      {
        path: 'editor_page',
        name: 'editor_page',
        meta: {
          icon: 'ios-create',
          title: '富文本编辑器',
          access: ['super_admin']
        },
        component: () => import('@/views/components/editor/editor.vue')
      },
      {
        path: 'icons_page',
        name: 'icons_page',
        meta: {
          icon: '_bear',
          title: '自定义图标',
          access: ['super_admin']
        },
        component: () => import('@/views/components/icons/icons.vue')
      }
    ]
  },
  {
    path: '/update',
    name: 'update',
    meta: {
      icon: 'md-cloud-upload',
      title: '数据上传',
      access: ['super_admin']
    },
    component: Main,
    children: [
      {
        path: 'update_table_page',
        name: 'update_table_page',
        meta: {
          icon: 'ios-document',
          title: '上传Csv',
          access: ['super_admin']
        },
        component: () => import('@/views/update/update-table.vue')
      },
      {
        path: 'update_paste_page',
        name: 'update_paste_page',
        meta: {
          icon: 'md-clipboard',
          title: '粘贴表格数据',
          access: ['super_admin']
        },
        component: () => import('@/views/update/update-paste.vue')
      }
    ]
  },
  {
    path: '/excel',
    name: 'excel',
    meta: {
      icon: 'ios-stats',
      title: 'EXCEL导入导出',
      access: ['super_admin']
    },
    component: Main,
    children: [
      {
        path: 'upload_excel',
        name: 'upload_excel',
        meta: {
          icon: 'md-add',
          title: '导入EXCEL',
          access: ['super_admin']
        },
        component: () => import('@/views/excel/upload-excel.vue')
      },
      {
        path: 'export_excel',
        name: 'export_excel',
        meta: {
          icon: 'md-download',
          title: '导出EXCEL',
          access: ['super_admin']
        },
        component: () => import('@/views/excel/export-excel.vue')
      }
    ]
  },
  {
    path: '/tools_methods',
    name: 'tools_methods',
    meta: {
      icon: 'ios-hammer',
      title: '工具方法',
      access: ['super_admin']
    },
    component: Main,
    children: [
      {
        path: 'tools_methods_page',
        name: 'tools_methods_page',
        meta: {
          icon: 'ios-hammer',
          title: '工具方法',
          access: ['super_admin'],
          beforeCloseName: 'before_close_normal'
        },
        component: () => import('@/views/tools-methods/tools-methods.vue')
      }
    ]
  },
  {
    path: '/directive',
    name: 'directive',
    meta: {
      icon: 'ios-navigate',
      title: '指令',
      access: ['super_admin']
    },
    component: Main,
    children: [
      {
        path: 'directive_page',
        name: 'directive_page',
        meta: {
          icon: 'ios-navigate',
          title: '指令',
          access: ['super_admin']
        },
        component: () => import('@/views/directive/directive.vue')
      }
    ]
  },
  {
    path: '/multilevel',
    name: 'multilevel',
    meta: {
      icon: 'md-menu',
      title: '多级菜单',
      access: ['super_admin']
    },
    component: Main,
    children: [
      {
        path: 'level_2_1',
        name: 'level_2_1',
        meta: {
          icon: 'md-funnel',
          title: '二级-1',
          access: ['super_admin']
        },
        component: () => import('@/views/multilevel/level-2-1.vue')
      },
      {
        path: 'level_2_2',
        name: 'level_2_2',
        meta: {
          icon: 'md-funnel',
          showAlways: true,
          title: '二级-2',
          access: ['super_admin']
        },
        component: parentView,
        children: [
          {
            path: 'level_2_2_1',
            name: 'level_2_2_1',
            meta: {
              icon: 'md-funnel',
              title: '三级',
              access: ['super_admin']
            },
            component: () => import('@/views/multilevel/level-2-2/level-3-1.vue')
          }
        ]
      },
      {
        path: 'level_2_3',
        name: 'level_2_3',
        meta: {
          icon: 'md-funnel',
          title: '二级-3',
          access: ['super_admin']
        },
        component: () => import('@/views/multilevel/level-2-3.vue')
      },
    ]
  },
  {
    path: '/tables',
    name: 'tables',
    meta: {
      icon: 'md-grid',
      title: '表格',
      access: ['super_admin']
    },
    component: Main,
    children: [
      {
        path: 'dragableTable',
        name: 'dragable-table',
        meta: {
          icon: 'ios-infinite',
          title: '可拖拽排序',
          access: ['super_admin']
        },
        component: () => import('@/views/tables/dragable-table.vue')
      },
      {
        path: 'editableTable',
        name: 'editable-table',
        meta: {
          icon: 'ios-create',
          title: '可编辑表格',
          access: ['super_admin']
        },
        component: () => import('@/views/tables/editable-table.vue')
      },
      {
        path: 'searchableTable',
        name: 'searchable-table',
        meta: {
          icon: 'ios-search',
          title: '可搜索表格',
          access: ['super_admin']
        },
        component: () => import('@/views/tables/searchable-table.vue')
      },
      {
        path: 'exportableTable',
        name: 'exportable-table',
        meta: {
          icon: 'ios-cloud-download',
          title: '表格导出数据',
          access: ['super_admin']
        },
        component: () => import('@/views/tables/exportable-table.vue')
      },
      {
        path: 'table2image',
        name: 'table-to-image',
        meta: {
          icon: 'ios-images',
          title: '表格转图片',
          access: ['super_admin']
        },
        component: () => import('@/views/tables/table-to-image.vue')
      }
    ]
  },
  {
    path: '/form',
    name: 'form',
    meta: {
      icon: 'md-create',
      title: '表单编辑',
      access: ['super_admin']
    },
    component: Main,
    children: [
      {
        path: 'artical-publish',
        name: 'artical-publish',
        meta: {
          icon: 'ios-paper',
          title: '文章发布',
          access: ['super_admin']
        },
        component: () => import('@/views/form/article-publish/article-publish.vue')
      },
      {
        path: 'workflow',
        name: 'workflow',
        meta: {
          icon: 'ios-swap',
          title: '工作流',
          access: ['super_admin']
        },
        component: () => import('@/views/form/work-flow/work-flow.vue')
      }
    ]
  },
  {
    path: '/advanced-router',
    name: 'advanced-router',
    meta: {
      icon: 'md-infinite',
      title: '高级路由',
      access: ['super_admin']
    },
    component: Main,
    children: [
      {
        path: 'mutative-router',
        name: 'mutative-router',
        meta: {
          icon: 'ios-link',
          title: '动态路由',
          access: ['super_admin'],
          beforeCloseName: 'before_close_normal'
        },
        component: () => import('@/views/advanced-router/mutative-router.vue')
      },
      {
        path: 'argument-page',
        name: 'argument-page',
        meta: {
          icon: 'ios-send',
          title: '带参页面',
          access: ['super_admin']
        },
        component: () => import('@/views/advanced-router/argument-page.vue')
      }
    ]
  }
]
