export default {
  components: '组件',
  count_to_page: '数字渐变',
  tables_page: '多功能表格',
  split_pane_page: '分割窗口',
  markdown_page: 'Markdown編輯器',
  editor_page: '富文本編輯器',
  icons_page: '自定義圖標',
  img_cropper_page: '圖片編輯器',
  update: '上傳數據',
  join: 'QQ群',
  join_page: 'QQ群',
  doc: '文檔',
  update_table_page: '上傳CSV文件',
  update_paste_page: '粘貼表格數據',
  multilevel: '多级菜单',
  directive: '指令',
  directive_page: '指令',
  level_1: 'level-1',
  level_2: 'level-2',
  level_2_1: 'level-2-1',
  excel: 'Excel導入導出',
  upload_excel: '導入Excel',
  export_excel: '導出Excel',
  tools_methods: '工具方法',
  tools_methods_page: '工具方法',
  home_v1: '首頁_V1',
  home_v1_page: '首頁_V1',
  component_v1: '組件_V1',
  component_v2: '組件_V2',
  'file-upload': '文件上傳',
  'area-linkage': '城市級聯',
  'draggable-list': '可拖拽列表',
  'home': '首頁_V2',
  'argument-page': '帶參頁面',
  'mutative-router': '動態路由',
  workflow: '工作流',
  'artical-publish': '文章發佈',
  'table-to-image': '表格轉圖片',
  'exportable-table': '表格導出數據',
  'searchable-table': '可搜索表格',
  'editable-table': '可編輯表格',
  'dragable-table': '可拖拽排序',
  'count-to': '數字漸變',
  'split-pane-page': '分個窗口',
  'drag_list_page': '可拖拽列表',
  'tables': '表格',
  'advanced-router': '高級路由',
  'form': '表單編輯',
  'level_2_3': '二級-3',
  'level_2_2': '二級-2',
  'level_2_2_1': '三級-2-1',
  'userManagement': '用戶管理',
  'user': '人員管理',
  'role': '角色管理',
  'resource': '菜單資源',
  'organization': '組織機構',
  'confManagement': '基礎數據',
  'dictionary': '公共碼錶',
  'fileAttach': '文件管理',
  'codeManagement': '代碼平台',
  'api': '接口文檔',
  'code': '代碼生成',
  'monitorManagement': '日誌監控',
  'log': '操作日誌',
  'druid': '數據監控',
  'monitor': '接口監控',
  'params': '動態路由',
  'query': '帶參路由',
  'shopping': '購物詳情',
  'order-info': '訂單詳情'
}
