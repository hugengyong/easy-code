import { setToken } from '@/libs/util'

export default {
  setAvator (state, avatorPath) {
    state.avatorImgPath = avatorPath
  },
  setUserId (state, id) {
    state.userId = id
  },
  setUserName (state, name) {
    state.userName = name
  },
  setAccess (state, access) {
    state.access = access
  },
  setToken (state, token) {
    state.token = token
    setToken(token)
  },
  setHasGetInfo (state, status) {
    state.hasGetInfo = status
  }
}
