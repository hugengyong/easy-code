import getters from './getters'
import actions from './actions'
import mutations from './mutations'
import { getToken } from '@/libs/util'

export default {
  state: {
    userName: '',
    userId: '',
    avatorImgPath: '',
    token: getToken(),
    access: '',
    hasGetInfo: false
  },
  getters,
  actions,
  mutations
}
