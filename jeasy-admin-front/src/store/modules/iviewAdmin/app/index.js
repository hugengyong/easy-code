import getters from './getters'
import actions from './actions'
import mutations from './mutations'
import { getHomeRoute } from '@/libs/util'
import { iviewAdminRouter } from '@/router/iviewAdmin'

export default {
  state: {
    breadCrumbList: [],
    tagNavList: [],
    homeRoute: getHomeRoute(iviewAdminRouter),
    local: ''
  },
  getters,
  actions,
  mutations
}
