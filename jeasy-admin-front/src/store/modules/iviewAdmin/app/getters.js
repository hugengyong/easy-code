import { getMenuByRouter } from '@/libs/util'
import { routers } from '@/router'

export default {
  menuList: (state, getters, rootState) => getMenuByRouter(routers, rootState.iviewAdminUserStore.access, rootState.loginStore.urlMenuMap)
}
